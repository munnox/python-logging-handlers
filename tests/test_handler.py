
import logging
import os
from logging_handlers import setup_opensearch_handler
from dotenv import load_dotenv


def test_main():
    uri = "admin:admin@localhost:9200"
    logging_index = "logging_index"

    load_dotenv()  # take environment variables from .env.
    os.environ.setdefault("OPENSEARCH_LOGGING_URI", uri)
    os.environ.setdefault("OPENSEARCH_LOGGING_INDEX", logging_index)
    
    uri = os.getenv("OPENSEARCH_LOGGING_URI", uri)
    logging_index = os.getenv("OPENSEARCH_LOGGING_INDEX", logging_index)
    opensearch_handler = setup_opensearch_handler(uri, logging_index, index_clear=True, debug=True, extra={"MAIN_TEST_KEY": "Value for all logged events"})
    logger = logging.getLogger()
    logger.addHandler(opensearch_handler)
    print( opensearch_handler )
    assert opensearch_handler.uri == uri
    assert opensearch_handler.index_name == logging_index
    logger.setLevel(logging.DEBUG)
    logger.debug("Sending debug message")
    logger.info("Sending info message", extra={"foo": "bar"})
    logger.warning("Sending warning message")
    logger.error("Sending error message")
    logger.critical("Sending critical message")
