#!/bin/env python3
# Simple opensearch logging handler
# Author Robert Munnoch
# Started 2023_05_28
# From opensearchhandler.py from explore-django

import os
from logging_handlers import setup_opensearch_handler
import logging
from tests.test_handler import test_main

logger = logging.getLogger(__file__)

if __name__ == "__main__":
    test_main()
