# coding: utf-8
"""
A simple program to make a inventory of all the file under a root directory given

The Opensearch Logging Handler

author: Robert Munnoch started 23/04/2023
"""
from __future__ import print_function
from __future__ import division

import logging

logger = logging.getLogger(__name__)

# Import OpenSearch
from .opensearch import OpensearchHandler, setup_opensearch_handler