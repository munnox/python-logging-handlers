# coding: utf-8
"""
A simple program to make a inventory of all the file under a root directory given

The Opensearch Logging Handler

author: Robert Munnoch started 23/04/2023
"""
from __future__ import print_function
from __future__ import division
import datetime

import os
import logging
import json
from opensearchpy import OpenSearch

logger = logging.getLogger(__name__)


class OpensearchHandler(logging.Handler):
    def __init__(
        self,
        uri: str = "admin:admin@localhost:9200",
        index_name: str = "logging_index",
        extra: dict[str, any] = None,
        index_clear: bool=False,
        handler_debug: bool=False,
        **kwargs
    ):
        default_level = 0
        if 'level' in kwargs:
            default_level = kwargs['level']
        super().__init__(level=default_level)
        self.uri = uri
        self.index_name = index_name
        self.index_config = {"settings": {"index": {"number_of_shards": 4}}}
        self.extra = extra
        self.index_clear = index_clear
        self.handler_debug = handler_debug

        self.use_ssl = True
        self.verify_certs = False
        self.assert_hostname = False
        self.ssl_show_warn = False

        if self.handler_debug:
            print(f"Opensearch Handler URI: {self.uri}")
        self.initialise()

    def initialise(self):
        self.opensearch = OpenSearch(
            [self.uri],
            use_ssl=self.use_ssl,
            verify_certs=self.verify_certs,
            ssl_assert_hostname=self.assert_hostname,
            ssl_show_warn=self.ssl_show_warn,
        )
        # index_body = {"settings": {"index": {"number_of_shards": 4}}}
        index_exists = self.opensearch.indices.exists(self.index_name)
        if self.handler_debug:
            print(
                f"{self.__class__.__name__} Index '{self.index_name}' exists?: {index_exists}"
            )
        if index_exists and self.index_clear:
            response = self.opensearch.indices.delete(self.index_name)
            if self.handler_debug:
                print(
                    f"{self.__class__.__name__} Index '{self.index_name}' deleted response: {response}"
                )
            index_exists = self.opensearch.indices.exists(self.index_name)
        if not index_exists:
            response = self.opensearch.indices.create(self.index_name, body=self.index_config)
            if self.handler_debug:
                print(
                    f"{self.__class__.__name__} Index '{self.index_name}' created response: {response}"
                )

    def emit(self, record):
        # import tzlocal
        # import pytz
        from zoneinfo import ZoneInfo

        now = datetime.datetime.now(tz=ZoneInfo("Europe/London"))
        record_created = datetime.datetime.fromtimestamp(
            record.created, tz=ZoneInfo("Europe/London")
        )
        try:
            document = {}
            # print(self.extra)
            if self.extra is not None and self.extra is dict:
                print(f"adding extra {self.extra}")
                document = self.extra
            document.update(record.__dict__)
            document["__class__.__name__"] = self.__class__.__name__
            document['@timestamp'] = record_created.isoformat()
            del document['created']
            document["formatted_message"] = self.format(record)
            # {
            #     # 'record': repr(record),
            #     "__class__.__name__": self.__class__.__name__,
            #     "logger_name": record.name,
            #     # "timestamp": now.isoformat(),
            #     "@timestamp": record_created.isoformat(),
            #     "relativeCreated": record.relativeCreated,
            #     "msg": record.msg,
            #     "formatted_message": self.format(record),
            #     "levelname": record.levelname,
            #     "levelno": record.levelno,
            #     "args": record.args,
            #     "module": record.module,
            #     "filename": record.filename,
            #     "funcName": record.funcName,
            #     "lineno": record.lineno,
            #     "pathname": record.pathname,
            #     "process": record.process,
            #     "processName": record.processName,
            #     "thread": record.thread,
            #     "threadName": record.threadName,
            #     "stack_info": record.stack_info,
            #     "exc_info": record.exc_info,
            #     "exc_text": record.exc_text,
            # }
            document['getmessage'] = record.getMessage()
            id = now.isoformat().replace(":", "").lower()
            if self.handler_debug:
                print(f"Document {document['levelname']} @ {document['@timestamp']}:\n{document}")
        except Exception as error:
            print(f"Opensearch Logging Handler translation error: {error}, {dir(record)} {record.args}")
            raise

        try:
            # self.opensearch.index
            response = self.opensearch.index(
                index=self.index_name, body=document, id=id, refresh=True
            )
        except Exception as error:
            print(f"Opensearch Logging Handler save to index error: {error}, {self.uri} - {self.index_name}\nid: {id}\ndocument:\n{document}")
            raise


def setup_opensearch_handler(uri=None, logging_index="logger_index", extra=None, debug=False,index_clear=False):
    # uri = os.getenv("OPENSEARCH_LOGGING_URI", uri)
    # logging_index = os.getenv("OPENSEARCH_LOGGING_INDEX", logging_index)
    # print("Using OpenSearch Logging")
    # Can Specify a Opensearch URI Enviroment variable as https://admin:admin@localhost:9200
    ophand = OpensearchHandler(
        uri,
        logging_index,
        index_clear=index_clear,
        handler_debug=debug,
        extra=extra
        # extra={
        #     'subdict': {
        #         "test1": "val1",
        #         "test2": ["val2a", "val2b"],
        #     },
        #     # the following does not translate in to the opensearch document it is saved but not indexed.
        #     'sublist': [
        #         {
        #             "test1": "val1",
        #         },
        #         {
        #             "test2": ["val2a", "val2b"],
        #         }
        #     ]
        # },
    )
    # ophand.setLevel(logging.INFO)
    # logging.getLogger().addHandler(ophand)
    # Suppress the logging from the opensearch api
    logging.getLogger("opensearch").setLevel(logging.WARNING)
    logging.getLogger("urllib3.connectionpool").setLevel(logging.INFO)
    return ophand
