# Opensearch Logging Handler

Extracting the Opensearch logging Handler out of other project into its own package so it can just be used when needed.

## Installation

```shell
pip install git+https://gitlab.com/munnox/python-logging-handlers.git@main
poetry add git+https://gitlab.com/munnox/python-logging-handlers.git@main
```


## Development

```shell
git clone https://gitlab.com/munnox/python-logging-handlers.git

poetry install

poetry run python logginghandler_opensearch
```